document.addEventListener('DOMContentLoaded', () => {
    let items = document.getElementsByClassName('item');
    let pinkTarget = document.getElementsByClassName('target')[0].lastElementChild;
    let RedTarget = document.getElementsByClassName('target')[0].lastElementChild;
    let compteurPink = 0;
    let compteurRed = 0;
    for (let item of items) {
        let cible = item.lastElementChild;
        if (cible.textContent === 'cible 1') {
            item.classList.replace('light-grey', 'pink');
            compteurPink++;
            pinkTarget.innerText = compteurPink;
        }
        if(cible.textContent === 'cible 2'){
            item.classList.replace('light-grey', 'red');
            compteurRed++;
            RedTarget.innerText = compteurRed;
        }
    }
})